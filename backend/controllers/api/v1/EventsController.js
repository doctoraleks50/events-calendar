'use strict'
const di = require('../../../di').container;

class EventsController {
    constructor(eventsService) {
        this.__eventsService = eventsService;
    }

    getById(req, res) {
        const result = this.__eventsService.getById(req.params.id);
        res.status(200).json(result);
    }

    list(req, res) {
        const result = this.__eventsService.list();
        res.status(200).json(result);
    }
}

module.exports = new EventsController(di.EventsService);