'use strict';

const UserModel = require('./User');
const EventModel = require('./Event');

module.exports = {
    UserModel,
    EventModel,
};
