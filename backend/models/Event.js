const {Schema, model, Types} = require('mongoose')

const schema = new Schema({
    title: {type: String, required: true},
    data: {type: String, required: true, unique: true},
    isDone: {type: String, required: true, unique: true},
    time: {type: Date, default: Date.now},
    user: {type: Types.ObjectId, ref: 'User'}
})

module.exports = model('Event', schema)