const di = require('bottlejs').pop('app');
const models = require('./models/');
const services = require('./services');

di.service('UsersService', function () {
    return new services.UsersService(models.UserModel);
});

di.service('EventsService', function () {
    return new services.EventsService(models.EventModel);
});

module.exports = di;
