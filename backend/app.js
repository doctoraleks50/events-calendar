const express = require('express')
const router = require('./routes');
const config = require('config');
const mongoose = require('mongoose');
const PORT = config.get('port') || 5000
const url = config.get('mongoUri')
const app = express()

app.use(express.json({ extended: true }))

// app.use('/api/v1', authorizationMiddleware().unless({
//     path: [
//         /\/auth/,
//         '/api/v1/users/profile'
//     ]
// }));

app.use(router);

// app.use('/api/auth', require('./routes/auth.routes'))
// app.use('/api/events', require('./routes/events.routes'))
// app.use('/t', require('./routes/redirect.routes'))

// if (process.env.NODE_ENV === 'production') {
//     app.use('/', express.static(path.join(__dirname, 'client', 'build')))
//
//     app.get('*', (req, res) => {
//         res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
//     })
// }

try {
    mongoose.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })
    app.listen(PORT, () => console.log(`App has been started on port ${PORT}...`))
} catch (e) {
    console.log('Server Error', e.message)
    process.exit(1)
}

module.exports = app


