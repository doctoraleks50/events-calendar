'use strict'

class UsersService {
    constructor(userModel) {
        this.__userModel = userModel;
    }

    getByEmail(email) {
        return this.__userModel.findOne({email});
    }

    async register(email, password) {
        return new this.__userModel({
            email,
            password,
        }).save();
    }

}

module.exports = UsersService;
