// const result = await Event.find({});
'use strict'

class EventsService {
    constructor(eventModel) {
        this.__eventModel = eventModel;
    }

    getById(id) {
        return this.__eventModel.findOne({
            _id: id
        });
    }

    list() {
        return new this.__eventModel({});
    }

}

module.exports = EventsService;
