const UsersService = require('./UsersService')
const EventsService = require('./EventsService')

module.exports = {
    UsersService,
    EventsService,
};
