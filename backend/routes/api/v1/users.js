const router = require('express').Router();
const { check } = require('express-validator');
const controllers = require('../../../controllers');
const UsersController = controllers.api.v1.UsersController;

// /api/v1/users/register
router.post(
    '/register',
    check('email', 'Некорректный email').isEmail(),
    check('password', 'Минимальная длина пароля 6 символов').isLength({min: 6}),
    UsersController.register.bind(UsersController)
);

// /api/v1/users/login
router.post(
    '/login',
    check('email', 'Введите корректный email').normalizeEmail().isEmail(),
    check('password', 'Введите пароль').exists(),
    UsersController.login.bind(UsersController),
);

module.exports = router