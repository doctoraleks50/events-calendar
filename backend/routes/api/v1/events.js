const router = require('express').Router();
const controllers = require('../../../controllers');
const EventsController = controllers.api.v1.EventsController;
const authMiddleware = require('../../../middlewares/auth.midleware');
//api/v1/events
router.get('/', authMiddleware, EventsController.list.bind(EventsController));
router.get('/:id', authMiddleware, EventsController.getById.bind(EventsController));

// router.post('/', async (req, res, next) => {
//     const newEvent = new Event(req.body.title, req.body.data, req.body.time, req.body.isDone);
//     await newEvent.save();
//     res.status(200).json(newEvent)
//
// })
// router.put('/:id', (req, res, next) => {
//     //todo
//
// })
// router.delete('/:id', async (res, next) => {
//     await Event.deleteOne({
//         '_id': req.params.id
//     })
// });

module.exports = router;